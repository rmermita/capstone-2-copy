const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController))
});

router.put('/admin', (req, res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send(`You are unauthorized to edit this.`)
		return;
	}
	const data = {
		userId : req.body.userId,
		isAdmin : req.body.isAdmin
	}
	userController.updateAdminRole(data).then(resultFromController => res.send(resultFromController))
});

router.get('/all', auth.verify, (req,res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send(`You are not an admin.`)
		return;
	}
	userController.allUsers().then(resultFromController => res.send(resultFromController))
})

module.exports = router;