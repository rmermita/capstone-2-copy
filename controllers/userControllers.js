const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.registerUser = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return `Email is already registered`
		} else {
			let newUser = new User ({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				email : reqBody.email,
				mobileNo : reqBody.mobileNo,
				password : bcrypt.hashSync(reqBody.password, 10)
			})
			return newUser.save().then((user, err) => {
				if (err){
					return `Unable to add new user. Please try again.`
				} else {
					return user
				}
			})
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return `Can't find user!`
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { access : auth.createAccessToken(result)}
			} else {
				return `Incorrect password!`
			}
		}
	})
}

module.exports.updateAdminRole = (data) => {
	return User.findById(data.userId).then((result, err) => {
		result.isAdmin = data.isAdmin;

		return result.save().then((user, err) => {
			if (err) {
				return `Something went wrong!`
			} else {
				return user
			}
		})
	})
}

module.exports.allUsers = async () => {
	try {
		let users = await User.find({});
		return users;
	} catch (err) {
		console.log(err);
		return `There's a problem in your request.`
	}
}