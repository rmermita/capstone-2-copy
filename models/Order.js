const mongoose = require('mongoose');
const productOrderSchema = new mongoose.Schema({
	productId : {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Product',
		required: [true, 'Product ID is required']
	},

	quantity : {
		type: Number,
		default : 0
	}
})
const orderSchema = new mongoose.Schema({

	userId : {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: [true, 'User ID is missing from order']
	},
	
	totalAmount : {
		type: Number,
		default: 0
	},

	purchasedOn : {
		type: Date,
		default: null
	},

	products : {
		type:[
		{
			type: productOrderSchema
		}
		],
		validate: [arrayLimit, 'Please input at least one product.']
	}
});
function arrayLimit(val) {
	return val.length >= 1;
}
module.exports = mongoose.model('Order', orderSchema);