const mongoose = require('mongoose');
const productSchema = new mongoose.Schema({

	name : {
		type: String,
		required: [true, "Product name is required."],
	},

	description : {
		type: String,
		required: [true, "Please put a product description."]
	},

	price : {
		type: Number,
		required: [true, "Input the product price."]
	},

	isActive : {
		type: Boolean,
		default: true,
	},

	createdOn : {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model('Product', productSchema);